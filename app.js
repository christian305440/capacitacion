//ejercicio1
/*
function f(x, y=2, z=7){
    return x+y+z;
}

console.log(f(5,undefined))*/

//ejercicio 2 
//var animal = 'kitty';
//var result = (animal === 'kitty') ? 'cute' : 'stille nice';

//console.log(result)

//ejercicio 3
/*var animal = 'kitty';
var result = '';
if (animal === 'kitty'){
    result = 'cute';
}else{
    result='still nice';
}
console.log(result)*/


//ejercicio 4 
/*var a = 0;
var str = 'not a';
var b = '';
b = a === 0 ? (a = 1, str+= ' test'):(a=2);

console.log(b)*/


//ejercicio 5
/*var a=1;
a === 1 ? alert ('Hey this is 1!'):0;

console.log(a)*/


//ejercicio 6 
/*a === 1 ? alert('hey, it is 1!')  : alert('Weird, what could it be ?');
if (a === 1) alert('hey, it is 1!') else alert ('Weird, what could it be?');
console.log(a);*/

//ejercicio 7 

/*var animal = 'kitty';
for (var i = 0; i < 5; ++i){
    (animal === 'kitty') ? 
    break: break:console.log(i);
}
// no se puede resolver porque no es posible usar declaraiones de control en los ternarios 
*/

/*var animal = 'kitty';
var result = (animal === 'kitty') ? 'meow' : 'woof'

console.log(result)
*/

//ejercicio 8
/*var value = 2;
switch (value) {
    case 1: 
    console.log('I will always run');
    break;
    case 2:
    console.log('I will never run')
    break;
}*/

//ejercicio9 
/*var animal = 'Lion';
switch (animal){
    case 'Dog':
        console.log('I will not run since animal !== "Dog"');
        break;
    case 'Cat':
        console.log('I will not run since animal !== "Cat"');
        break;
        default:
            console.log('I will run since animal does not mach any other case');
}*/

// ejercicio10
/*function john() {
    return 'John';
  }
  
function jacob() {
    return 'Jacob';
  }
  
  switch (name) {
    case john(): // Compare name with the return value of john() (name == "John")
      console.log('I will run if name === "John"');
      break;
    case 'Ja' + 'ne': // Concatenate the strings together then compare (name == "Jane")
      console.log('I will run if name === "Jane"');
      break;
    case john() + ' ' + jacob() + ' Jingleheimer Schmidt':
      console.log('His name is equal to name too!');
      break;
  }*/

  //ejercicio11
/*
var x= 'e'
switch (x) {
    case "a":
    case "b":
    case "c":
        console.log("Either a, b, or c was selected.");
        break;
case "d":
    console.log("Only d was selected");
    break;
    default:
        console.log("No case was metched.");
        break;
    
}*/


//ejercicio12
//var x = 5+7 ;=12
//var x = 5 + "7" ;=57
//var x = "5" + 7 ;
//var x = 5-7;=-2
//var x = 5 - "7";=-2
//var x = "5" - 7; =-2
//var x = 5 - "x"; =NaN


//ejercicio 13
//var a = 'hello'|| " "; 
//var b = '' || [] ;
//var c = '' || undefined ;
//var d = 1 || 5 ;
//var e = 0 || {};
//var f = 0 || '' || 5 ;
//var g = '' || 'yay' || 'boo' ;

//console.log(g);

//ejercicio14
//var a = 'hello' && ''; 
//var b = '' && [] ;
//var c =  undefined && 0 ;
//var d = 1 && 5 ;
//var e = 0 && {};
//var f = 'hi' && [] && 'done' ;
//var g = 'bye' && undefined ;

//console.log(a);

//ejercico 15

//var foo = function(val) {
  //  return val || 'default';
//}
//console.log(foo('burger'));
//console.log(foo('100'));
//console.log(foo('[]'));
//console.log(foo('0'));
//console.log(foo('undefined'));

// ejercicio 16
//var isLegal = age >= 18;
//var tall = height >=5.11;
//var suitable = isLegal && tall;
//var isRoyalty = statu === 'royalty'
//var specialCase = isRoyalty && hasInvitation;
//var cantEnterOurBar = suitable || specialCase
//console.log();

//ejercico17
/*for (var i=0; i <=3; i++){
    if(i === 1){
        continue;
    }
    console.log(i);
}*/

//ejercicio18

/*var i = 0;
while ( i < 3) {
    if ( i === 1)
    {
        i = 2;
        continue;
    }
    console.log(i);
    i++
}*/

//ejercico19
/*for(var i = 0; i <5 ; i++){
    nextLoop2Interation:
    for(var j = 0; j < 5 ; j++ ){
        if(i==j) break nextLoop2Interation;
        console.log(i,j)
    }
}*/

//ejercicio20
/*function foo(){
    var a = 'hello';

    function bar( ){
        var b ='world';
        console.log(a);
        console.log(b);
    }
 
}*/

//jercicio21
/*function foo(){
    const a = true;

    function bar(){
        const a =false;
        console.log(a);
    }
    const a= false ;
    a = false; 
    console.log(a);
}*/

//ejercico22
/*var namedSum = function sum(a,b) {
    return a+b;
}

var anonSum = function (a,b) {
    return a+b;
}
console.log(namedSum(1,3));
console.log(anonSum(1,3));*/

//ejercicio 23
/*var a = [1,2,3,8,9,10];
a.slice(0,3).contact ([4,5,6,7]), a.slice(3,6);

var a = [1,2,3,8,9,10];
a.splice(3,0, ... [4,5,6,7]);
console.log(a);*/


//ejercico24
/*var array =['a', 'b' , 'c']
array.join('->');
array.join('.');
'a,b,c'.split('.')
'5.4.3.2.1'.split('.')

var division= array.split;
console.log(division);*/


//ejercico25
//var array =['a','b','c','d','e','f'];
//copy=array.copyWithin(4,0,1);
//console.log(copy);

/*var array =['a','b','c','d','e','f'];
copy=array.copyWithin(3,0,3);
console.log(copy);*/

/*var array =['a','b','c','d','e','f'];
copy=array.fill('Z',0,5);
console.log(copy);*/


//ejercico26
//var array = [5,10,15,20,25];
//comp=Array.isArray(array)// comprueba si es un array
//console.log(comp)

//var array = [5,10,15,20,25];
//comp=array.includes(10)// comprueba si obj esta en el array
//console.log(comp)

//var array = [5,10,15,20,25];
//comp=array.includes(10,2)// comprueba si obj esta en el array
//console.log(comp)

//var array = [5,10,15,20,25];
//comp=array.indexOf(10)// devuelve la primera aparicion 
//console.log(comp)

//var array = [5,10,15,20,25];
//comp=array.lastIndexOf(10,0)// devuelve la aparicion de la ultima aparicion 
//console.log(comp)


//ejercicio27
/*
var array = ['a', 'b', 'c', 'd', 'e', 'f'];

console.log(array.copyWithin(5,0,1));
console.log(array.copyWithin(3,0,3));
console.log(array.fill('Z',0,5)); 


var array = ['Anna' , 'Alberto' , 'Maurio','Bernardo', 'Zoe'];
abe=array.sort();//lo ordena de forma Alfabetica descendente
console.log(abe);*/

//var array = ['Anna' , 'Alberto' , 'Maurio','Bernardo', 'Zoe'];
//abe=array.reverse();//lo ordena de forma Alfabetica ascendente
//console.log(abe);










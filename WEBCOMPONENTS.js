obtenerEmp();

async function obtenerEmp() {
  try {
    const url = "http://dummy.restapiexample.com/api/v1/employees";
    const response = await fetch(url);
    const datos = await response.json();
    let empleados = datos.data;
    mostrarEmpleados(empleados);
  } catch (error) {
    console.log(error);
  }
}

async function buscarEmp(nombre) {
  try {
    const url = "http://dummy.restapiexample.com/api/v1/employees";
    const response = await fetch(url);
    const datos = await response.json();
    let empleados = datos.data;
    for (let obj of empleados) {
      if (obj.employee_name == nombre) {
        let tbBody = document.querySelector("tbody");
        tbBody.textContent = "";
        let filab = document.createElement("tr");
        for (let key in obj) {
          let columnab = document.createElement("td");
          let texto = document.createTextNode(obj[key]);
          columnab.appendChild(texto);
          filab.appendChild(columnab);
        }
        tbBody.appendChild(filab); 
        return
      } 
    }
    alert('Nombre no encontrado')
   
  } catch (error) {
    console.log(error);
  }
}

function mostrarEmpleados(empleados) {
    let tbBody = document.querySelector("tbody");
    for (let obj of empleados) {
      let filab = document.createElement("tr");
      for (let key in obj) {
        let columnab = document.createElement("td");
        let texto = document.createTextNode(obj[key]);
        columnab.appendChild(texto);
        filab.appendChild(columnab);
      }
      tbBody.appendChild(filab);
    }
  }

class MiBotonExtendidoSearch extends HTMLButtonElement {
  constructor() {
    super();
    this.addEventListener("click", (e) => {
      let campo = document.querySelector("#buscar");
      let texto = campo.value;
      buscarEmp(texto.trim());
    });
  }
  static get ceName() {
    return "mi-boton-search";
  }
  get is() {
    return this.getAttribute("is");
  }
  set is(value) {
    this.setAttribute("is", value || this.ceName);
  }
}

class MiBotonExtendidoClear extends HTMLButtonElement {
  constructor() {
    super();
    this.addEventListener("click", (e) => {
      let campo = document.querySelector("#buscar");
      let tab=document.querySelector("tbody");
      campo.value = "";
      tab.textContent=''
      console.log("evento click " + this.innerHTML);
      obtenerEmp();
    });
  }
  static get ceName() {
    return "mi-boton-clear";
  }
  get is() {
    return this.getAttribute("is");
  }
  set is(value) {
    this.setAttribute("is", value || this.ceName);
  }
}
customElements.define("mi-boton-search", MiBotonExtendidoSearch, {
  extends: "button",
});
customElements.define("mi-boton-clear", MiBotonExtendidoClear, {
  extends: "button",
});
